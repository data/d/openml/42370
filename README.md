# OpenML dataset: yacht_hydrodynamics

https://www.openml.org/d/42370

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ship Hydromechanics Laboratory","Maritime and Transport Technology Department","Technical University of Delft.  
**Source**: UCI - [original](http://archive.ics.uci.edu/ml/datasets/yacht+hydrodynamics) - Date unknown  
**Please cite**:   

**Yacht Hydrodynamics Dataset**

**Data Set Information**

Prediction of residuary resistance of sailing yachts at the initial design stage is of a great value for evaluating the ship&acirc;&euro;&trade;s performance and for estimating the required propulsive power. Essential inputs include the basic hull dimensions and the boat velocity.

The Delft data set comprises 308 full-scale experiments, which were performed at the Delft Ship Hydromechanics Laboratory for that purpose.
These experiments include 22 different hull forms, derived from a parent form closely related to the &acirc;&euro;&tilde;Standfast 43&acirc;&euro;&trade; designed by Frans Maas.


**Attribute Information**

Variations concern hull geometry coefficients and the Froude number:

1. Longitudinal position of the center of buoyancy, adimensional.
2. Prismatic coefficient, adimensional.
3. Length-displacement ratio, adimensional.
4. Beam-draught ratio, adimensional.
5. Length-beam ratio, adimensional.
6. Froude number, adimensional.

The measured variable is the residuary resistance per unit weight of displacement:

7. Residuary resistance per unit weight of displacement, adimensional.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42370) of an [OpenML dataset](https://www.openml.org/d/42370). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42370/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42370/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42370/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

